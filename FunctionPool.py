import os
import sys
from shutil import copyfile
import subprocess as subp

GPFTemplateFile = "Data" + os.sep + "template.gpf"

def creatGPF(pdbqtFile, outputGPF, pdbID):
    currentGPFFolder = outputGPF + os.sep + pdbID
    if not os.path.exists(currentGPFFolder):
        os.makedirs(currentGPFFolder)
    copyfile(pdbqtFile, currentGPFFolder + os.sep + pdbID + ".pdbqt")
    with open(GPFTemplateFile, 'r') as file :
        filedata = file.read()

    # Replace the target string
    filedata = filedata.replace('$$$$', currentGPFFolder + os.sep + pdbID)

    # Write the file out again
    with open(currentGPFFolder + os.sep + pdbID + '.gpf', 'w') as file:
        file.write(filedata)

    # If the OS is windows
    if os.name == 'nt':
        subp.check_call(["Tools" + os.sep + "autogrid4.exe","-p",currentGPFFolder + os.sep + pdbID + '.gpf'])
    # If the OS is Linux
    if os.name == 'posix':
        subp.check_call(["Tools" + os.sep + "autogrid4","-p",currentGPFFolder + os.sep + pdbID + '.gpf'])


def cleanPDB(pdbFile):
    with open(pdbFile, "r") as f:
        lines = f.readlines()
    with open(pdbFile, "w") as f:
        for line in lines:
            if "HETATM" not in line:
                f.write(line)


