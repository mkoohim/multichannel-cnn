import argparse
import glob
import os
import warnings
import sys
from Bio.PDB import PDBParser, is_aa
from Bio.PDB.Polypeptide import three_to_one
from propy import AAComposition as AAC
from propy import Autocorrelation as AC
from propy import CTD as CTD
from propy import QuasiSequenceOrder as QSO
from propy import ProCheck as PC

warnings.filterwarnings("ignore")

#Args
parser = argparse.ArgumentParser(description='GenerateSeqFeatures is script to generate sequential features '
                                             'of the metal binding pockets')
# Input
grpInput = parser.add_argument_group('Input:')
grpInput.add_argument('--PDBFolder', type=str, dest='strPDBFolder',
                      help='Enter the path of the folder that contain PDB files.')
# Output
grpOutput = parser.add_argument_group('Output:')
grpOutput.add_argument('--output', type=str, dest='strOut',
                       help='Enter the path of the output directory')

if len(sys.argv) == 1:
    parser.print_help()
    sys.stderr.write(
        "\nNo arguments were supplied. Please see the usage information above to determine what to pass to the program.\n")
    sys.exit(1)
args = parser.parse_args()

alldirs = glob.glob(args.strPDBFolder + os.sep + "*.pdb")
outputSeq = args.strOut
if not os.path.exists(outputSeq):
    os.makedirs(outputSeq)
tempCSVContent = ""
for pdbFile in alldirs:
    pdbID = pdbFile.split(os.sep)[-1].replace(".pdb","")
    p = PDBParser(PERMISSIVE=1)
    structure = p.get_structure(pdbFile, pdbFile)
    for model in structure:
        seq = list()
        for chain in model:
            chainID = chain.get_id()
            for residue in chain:
                if is_aa(residue.get_resname(), standard=True):
                    seq.append(three_to_one(residue.get_resname()))
        pocketSeq = "".join(seq)

    featureNames = []
    featureValues = []
    ProteinSequence = pocketSeq
    if(PC.ProteinCheck(ProteinSequence) > 0):
        #print "Protein A Composition (Percent) - 20"
        dicAAC = AAC.CalculateAAComposition(ProteinSequence)

        #print "Protein CTD - All - 147"
        dicCTD = CTD.CalculateCTD(ProteinSequence)

        #print "Protein Autocorrelation - All - 720"
        dicAC = AC.CalculateAutoTotal(ProteinSequence)

        #print "Protein Quasi-sequence order descriptors - 100"
        dicQSO = QSO.GetQuasiSequenceOrder(ProteinSequence)

        #print "Protein Sequence order coupling number descriptors - 60"
        dicQSO2 = QSO.GetSequenceOrderCouplingNumberTotal(ProteinSequence)

        dicAll = dict(dicAAC.items() + dicCTD.items() + dicAC.items() + dicQSO.items() + dicQSO2.items())

        featureNames = list(dicAll.keys())
        featureValues = list(dicAll.values())

        row = pdbID + ","
        for item in featureValues:
            row += str(item) + ","
        row = row.rstrip()
        row += "\n"
        tempCSVContent += row

header = "pdbID" + ","
for item in featureNames:
    header += str(item) + ","
header = header.rstrip()
header += "\n"
header += tempCSVContent
file = open(outputSeq + os.sep + "seqFeatures.csv","w")
file.write(header)
file.close()