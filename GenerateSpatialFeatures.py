import argparse
import glob
import os
import warnings
import sys
import FunctionPool as FP

warnings.filterwarnings("ignore")

#Args
parser = argparse.ArgumentParser(description='GenerateSpatialFeatures is script to generate five different '
                                             'energy-based affinity grid maps for each receptor')
# Input
grpInput = parser.add_argument_group('Input:')
grpInput.add_argument('--PDBQTFolder', type=str, dest='strPDBQTFolder',
                      help='Enter the path of the folder that contain PDBQT files.')
# Output
grpOutput = parser.add_argument_group('Output:')
grpOutput.add_argument('--output', type=str, dest='strOut',
                       help='Enter the path of the output directory')

if len(sys.argv) == 1:
    parser.print_help()
    sys.stderr.write(
        "\nNo arguments were supplied. Please see the usage information above to determine what to pass to the program.\n")
    sys.exit(1)
args = parser.parse_args()

alldirs = glob.glob(args.strPDBQTFolder + os.sep + "*.pdbqt")
outputGPF = args.strOut
if not os.path.exists(outputGPF):
    os.makedirs(outputGPF)

for pdbqtFile in alldirs:
    pdbID = pdbqtFile.split(os.sep)[-1].replace(".pdbqt","")
    FP.creatGPF(pdbqtFile, outputGPF, pdbID)