import argparse
import csv
#from string import upper
import sys

__author__ = 'wanglab'
import Bio.PDB
import os.path as op

#Args
parser = argparse.ArgumentParser(description='MetalCoordination is script to extract residues which are in the first'
                                             ' and second coordination of the metal binding sites')
# Input
grpInput = parser.add_argument_group('Input:')
grpInput.add_argument('--PDBFile', type=str, dest='strPDB',
                      help='Enter the path of the PDB.')
# Output
grpOutput = parser.add_argument_group('Output:')
grpOutput.add_argument('--output', type=str, dest='strOut',
                       help='Enter the path of the output CSV file')

if len(sys.argv) == 1:
    parser.print_help()
    sys.stderr.write(
        "\nNo arguments were supplied. Please see the usage information above to determine what to pass to the program.\n")
    sys.exit(1)
args = parser.parse_args()

a = args.strPDB
b = args.strPDB

mrtalList = ["ZN","MG","FE","CU","CA","NA","AS","HG","MN","K","SM","W","CO","NI","AU","CD"]
resList = ["ALA", "ARG", "ASN", "ASP", "CYS", "GLU", "GLN", "GLY", "HIS","ILE", "LEU", "LYS", "MET", "PHE", "PRO", "SER", "THR", "TRP","TYR", "VAL"]

model_a = Bio.PDB.PDBParser().get_structure(op.splitext(a)[0], a)[0]
model_b = Bio.PDB.PDBParser().get_structure(op.splitext(b)[0], b)[0]

b_residues = list(model_b.get_residues())

dicMetals = {}
for i, r in enumerate(b_residues):
    if r.get_resname().replace(" ", "") in mrtalList:
        dicMetals[i] = r.get_resname().replace(" ", "")

with open(args.strOut , 'w') as text_file:
    writer = csv.writer(text_file)
    writer.writerow(("PDB-ID","Chain-ID","Metal","Residue", "ResPos","Distance","Status"))
    for k,v in dicMetals.iteritems():
        for i, r in enumerate(model_a.get_residues()):
            if r.get_resname().replace(" ", "") in resList:
                dist = abs(r['CA'] - b_residues[k][v])
                if dist < 5:
                    writer.writerow((str(r.get_full_id()[0]), str(r.get_full_id()[2]), str(v), str(r.get_resname()), str(r.get_full_id()[3][1]), str(dist), "First Coordinate"))
                elif 5 <= dist < 10:
                    writer.writerow((str(r.get_full_id()[0]), str(r.get_full_id()[2]), str(v), str(r.get_resname()), str(r.get_full_id()[3][1]), str(dist), "Second Coordinate"))
