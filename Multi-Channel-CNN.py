import argparse
import glob
import pickle
from keras import Input, Model
from keras.layers import Dense, Conv3D, MaxPooling3D, Flatten, concatenate
from keras.optimizers import Adam
from keras.regularizers import l2
from keras.utils import plot_model
import pandas
from sklearn.metrics import roc_curve, auc
from scipy import interp
from sklearn.model_selection import StratifiedKFold
import matplotlib.pyplot as plt
import random
import numpy
import os
import sys
from keras import backend as K

numpy.random.seed(42)
random.seed(42)
os.environ["PYTHONHASHSEED"] = '0'


#Args
parser = argparse.ArgumentParser(description='Multi-Channel-CNN is script to build and evaluate a multi-channel'
                                             'convolution neural network to predict disease associated mutation of metal-binding site.'
                                             'We used 10-fold cross validation to evaluate our approach.')
# Input
grpInput = parser.add_argument_group('Input:')
grpInput.add_argument('--seqFeatures', type=str, dest='strSeqFeatures',
                      help='Enter the path of the sequential features. It should be in CVS format as describe above.')

grpInput.add_argument('--spatialFeatures', type=str, dest='strSpatialFeatures',
                      help='Enter the path of the Spatial features folder.')
grpInput.add_argument('--filters', type=int, dest='strFilters',
                      help='Enter the number of filters.', default=16)



# Output
grpOutput = parser.add_argument_group('Output:')
grpOutput.add_argument('--o', type=str, dest='strOut',
                       help='Enter the path of the output directory')

if len(sys.argv) == 1:
    parser.print_help()
    sys.stderr.write(
        "\nNo arguments were supplied. Please see the usage information above to determine what to pass to the program.\n")
    sys.exit(1)
args = parser.parse_args()

X_train_metadata = pandas.read_csv(args.strSeqFeatures)

#Shuffle the dataset
X_train_metadata = X_train_metadata.sample(frac=1).reset_index(drop=True)

X = X_train_metadata.iloc[:, 0:len(X_train_metadata.columns)-2].values
y = X_train_metadata.iloc[:, -1].values

from sklearn.preprocessing import LabelEncoder, OneHotEncoder
labelencoder_X_1 = LabelEncoder()

X[:, 0] = labelencoder_X_1.fit_transform(X[:, 0])
X[:, 1] = labelencoder_X_1.fit_transform(X[:, 1])
X[:, 3] = labelencoder_X_1.fit_transform(X[:, 3])
X[:, 4] = labelencoder_X_1.fit_transform(X[:, 4])
onehotencoder = OneHotEncoder(categorical_features = [0,1,3,4])
X = onehotencoder.fit_transform(X).toarray()

X = numpy.append(X, X_train_metadata.iloc[:, len(X_train_metadata.columns)-2].values.reshape(-1, 1), axis=1)


grid_rows, grid_cols, grid_depth = 15, 15, 15
def makeInputDataSet(sourceFolder, dirList,typeData):
    trainData = []
    for item in dirList:
        link = sourceFolder + os.sep + item
        files = glob.glob(link + os.sep + '*.'+typeData+'.map')
        a = 0
        mapFile = files[0]

        final_model = [ [ [ 0 for i in range(grid_rows) ]
                                  for j in range(grid_cols) ]
                                  for k in range(grid_depth) ]
        with open(mapFile) as f:
            form = []
            for _ in range(6):
                next(f)
            for line in f:
                form.append(float(line.rstrip()))

        for i in range(grid_rows):
            for j in range(grid_cols):
                for k in range(grid_depth):
                    final_model[i][j][k] = form[a]
                    a = a + 1
        trainData.append(final_model)
    trainData = numpy.array(trainData)
    if K.image_dim_ordering() == 'th':
        trainData = trainData.reshape(trainData.shape[0], 1, grid_rows, grid_cols, grid_depth)
    else:
        trainData = trainData.reshape(trainData.shape[0], grid_rows, grid_cols, grid_depth, 1)
    return trainData

def build_model():
    # Number of Convolutional Filters to use
    nb_filters = args.strFilters

    # Convolution Kernel Size
    kernel_size = [10,10,10]

    # channel 1
    #  3D Convolution layer
    inputs1 = Input(shape=input_shape)
    conv1_1 = Conv3D(nb_filters, kernel_size[0], kernel_size[1], kernel_size[2],
                            input_shape=input_shape,
                            activation='relu',kernel_regularizer=l2(0.01))(inputs1)
    pooling1 = MaxPooling3D(pool_size=(3, 3, 3))(conv1_1)
    flatten1 = Flatten()(pooling1)


    # channel 2
    #  3D Convolution layer
    inputs2 = Input(shape=input_shape)
    conv2_1 = Conv3D(nb_filters, kernel_size[0], kernel_size[1], kernel_size[2],
                            input_shape=input_shape,
                            activation='relu',kernel_regularizer=l2(0.01))(inputs2)
    # Fully Connected layer
    pooling2 = MaxPooling3D(pool_size=(3, 3, 3))(conv2_1)
    flatten2= Flatten()(pooling2)

    # channel 3
    #  3D Convolution layer
    inputs3 = Input(shape=input_shape)
    conv3_1 = Conv3D(nb_filters, kernel_size[0], kernel_size[1], kernel_size[2],
                            input_shape=input_shape,
                            activation='relu',kernel_regularizer=l2(0.01))(inputs3)
    pooling3 = MaxPooling3D(pool_size=(3, 3, 3))(conv3_1)
    flatten3= Flatten()(pooling3)

    # channel 4
    #  3D Convolution layer
    inputs4 = Input(shape=input_shape)
    conv4_1 = Conv3D(nb_filters, kernel_size[0], kernel_size[1], kernel_size[2],
                            input_shape=input_shape,
                            activation='relu',kernel_regularizer=l2(0.01))(inputs4)
    pooling4 = MaxPooling3D(pool_size=(3, 3, 3))(conv4_1)
    flatten4= Flatten()(pooling4)

    # channel 5
    #  3D Convolution layer
    inputs5 = Input(shape=input_shape)
    conv5_1 = Conv3D(nb_filters, kernel_size[0], kernel_size[1], kernel_size[2],
                            input_shape=input_shape,
                            activation='relu',kernel_regularizer=l2(0.01))(inputs5)
    pooling5 = MaxPooling3D(pool_size=(3, 3, 3))(conv5_1)
    flatten5= Flatten()(pooling5)

    inputs10 = Input(shape=(numpy.size(X,1)-1,))

    # Merge the output of five different channel with sequential features
    merged = concatenate([flatten1, flatten2, flatten3, flatten4, flatten5, inputs10])

    # Fully Connected layer
    dense2 = Dense(1024,init = 'uniform',
                   activation='relu',kernel_regularizer=l2(0.01))(merged)
    dense3 = Dense(512,init = 'uniform',
                   activation='relu',kernel_regularizer=l2(0.01))(dense2)
    dense4 = Dense(256,init = 'uniform',
                   activation='relu',kernel_regularizer=l2(0.01))(dense3)
    dense5 = Dense(128,init = 'uniform',
                  activation='relu',kernel_regularizer=l2(0.01))(dense4)
    dense6 = Dense(64,init = 'uniform',
                  activation='relu',kernel_regularizer=l2(0.01))(dense5)
    dense7 = Dense(32,init = 'uniform',
                  activation='relu',kernel_regularizer=l2(0.01))(dense6)
    dense8 = Dense(16,init = 'uniform',
                  activation='relu',kernel_regularizer=l2(0.01))(dense7)
    outputs = Dense(output_dim=1, init='uniform', activation='sigmoid')(dense8)

    model = Model(inputs=[inputs1, inputs2, inputs3, inputs4, inputs5, inputs10], outputs=outputs)
    opt = Adam(lr=0.0001)
    model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['binary_accuracy'])
    return model

cv = StratifiedKFold(n_splits=10)
tprs = []
aucs = []
mean_fpr = numpy.linspace(0, 1, 100)
model_grid = True
all_predic = []
all_y_cv = []

for train, test in cv.split(X, y):
    #Train sequential features
    X_train_metadata_cv = numpy.copy(X[train])
    X_train_metadata_cv = numpy.delete(X_train_metadata_cv, -1, axis=1)
    X_train_metadata_cv = numpy.array(X_train_metadata_cv)

    #Test sequential features
    X_test_metadata_cv = numpy.copy(X[test])
    X_test_metadata_cv = numpy.delete(X_test_metadata_cv, -1, axis=1)
    X_test_metadata_cv = numpy.array(X_test_metadata_cv)


    #Train spatial features
    X_train_A_cv = makeInputDataSet(args.strSpatialFeatures,X[train][:,-1],"A")
    X_train_C_cv = makeInputDataSet(args.strSpatialFeatures,X[train][:,-1],"C")
    X_train_e_cv = makeInputDataSet(args.strSpatialFeatures,X[train][:,-1],"e")
    X_train_HD_cv = makeInputDataSet(args.strSpatialFeatures,X[train][:,-1],"HD")
    X_train_OA_cv = makeInputDataSet(args.strSpatialFeatures,X[train][:,-1],"OA")

    #Test spatial features
    X_test_A_cv = makeInputDataSet(args.strSpatialFeatures,X[test][:,-1],"A")
    X_test_C_cv = makeInputDataSet(args.strSpatialFeatures,X[test][:,-1],"C")
    X_test_e_cv = makeInputDataSet(args.strSpatialFeatures,X[test][:,-1],"e")
    X_test_HD_cv = makeInputDataSet(args.strSpatialFeatures,X[test][:,-1],"HD")
    X_test_OA_cv = makeInputDataSet(args.strSpatialFeatures,X[test][:,-1],"OA")

    if K.image_dim_ordering() == 'th':
        input_shape = (1, grid_rows, grid_cols, grid_depth)
    else:
        input_shape = (grid_rows, grid_cols, grid_depth, 1)


    keras_model = build_model()
    keras_model.fit([X_train_A_cv, X_train_C_cv, X_train_e_cv, X_train_HD_cv, X_train_OA_cv, X_train_metadata_cv], y[train], epochs=10,batch_size=10)
    y_pred_keras = keras_model.predict([X_test_A_cv, X_test_C_cv, X_test_e_cv, X_test_HD_cv, X_test_OA_cv, X_test_metadata_cv]).ravel()

    y_pred = (y_pred_keras > 0.5)
    all_y_cv.append(y[test])
    all_predic.append(y_pred_keras)
    fpr_keras, tpr_keras, thresholds_keras = roc_curve(y[test], y_pred_keras)
    tprs.append(interp(mean_fpr, fpr_keras, tpr_keras))
    tprs[-1][0] = 0.0
    roc_auc = auc(fpr_keras, tpr_keras)
    print(roc_auc)
    aucs.append(roc_auc)

mean_tpr = numpy.mean(tprs, axis=0)
mean_tpr[-1] = 1.0
mean_auc = auc(mean_fpr, mean_tpr)
std_auc = numpy.std(aucs)
plt.plot(mean_fpr, mean_tpr,
             label=r'(AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             lw=1.5, alpha=.9)

plt.xlim([-0.05, 1.05])
plt.ylim([-0.05, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic for')
plt.legend(loc="lower right")
plt.show()


