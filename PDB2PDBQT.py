import argparse
import glob
import subprocess as subp
import warnings
import sys
import FunctionPool as FP
import os

warnings.filterwarnings("ignore")
#Args
parser = argparse.ArgumentParser(description='PDB2PDBQT is script to convert PDB structure of receptor to PDBQT')

# Input
grpInput = parser.add_argument_group('Input:')
grpInput.add_argument('--PDBFolder', type=str, dest='strPDBFolder',
                      help='Enter the path of the folder that contain PDB files.')

# Output
grpOutput = parser.add_argument_group('Output:')
grpOutput.add_argument('--outputPDBQT', type=str, dest='strOutPDBQT',
                       help='Enter the path of the output PDBQT directory')

grpPrograms = parser.add_argument_group('Programs:')
grpPrograms.add_argument('--ADTool', default=str("prepare_receptor4.py"), type=str, dest='strADT',
                         help='Provide the path to AutoDockTools. Default call will be prepare_receptor4.py.')

if len(sys.argv) == 1:
    parser.print_help()
    sys.stderr.write(
        "\nNo arguments were supplied. Please see the usage information above to determine what to pass to the program.\n")
    sys.exit(1)
args = parser.parse_args()

# Variables
allPDBs = glob.glob(args.strPDBFolder + os.sep + "*.pdb")
outputFolder = args.strOutPDBQT

if not os.path.exists(outputFolder):
    os.makedirs(outputFolder)

for pdbFile in allPDBs:
    # Extract PDB ID
    pdbID = pdbFile.split("\\")[-1].replace(".pdb","")

    # Clean PDB from small molecules
    FP.cleanPDB(pdbFile)

    # Convert PDB to PDBQT
    subp.check_call(["python", args.strADT, "-r", pdbFile , "-o", outputFolder + os.sep + pdbID + ".pdbqt"])

